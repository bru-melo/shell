#!/usr/bin/env bash

#Routines to prepare the directories required to run run_ami_3d
#Modified from Andrew Schaeffer codes. The idea is that we can make this step in one goal, for practicality #- also simplify a bit the proccess, as the original codes are written in tcsh and are (very) long.
#++++ plus adding the parallelization option.

#Future modifications:
#add overwrite option (now overwritting)
#send e-mail

USAGE () {
	cat << EOM
	###################################################################################
	-----------------------------------------------------------------------------------
	Program:        `basename ${0}`
	Authors:        Bruna Melo May 2019
	Purpose:        Run Ami DIrectories:
	Prepare the directories needed to run RAM3N. Run from project directory.

	Usage:		

	`basename ${0}`  [-d data_directory] [-a ami_directory] -p phase [-j jobsnum] [-m send mail]
					 [-l run link_ami_dirs] [-z dz_list_file_name]
					 [-f run firstP] [-k link_output]
					 [-n run rad_nod] [-o first_p_output]
					 [-R run RAM3n.sh]

			Options:
				-h	this help

				file/directory options:
				-d 	[optional; default data] data directory
				-a	[optional; default ami] ami directory
				-p	[required] run for (L)ove or (R)ayleigh
				-m  [optional] to send e-mail after finished.
				    Must have .script-config set up correctly

				job control options:
				-j	[optional; default 1] number of parallel jobs

				run separately options: default runs everything
				-l  [optional] runs linking the data directories step into ami folder
				-z 	[optional; default list] dz list file name (without .d[z,t])
				-f  [optional] runs only creating first P step
				-k	[optional; default link_ready_all.d[z,t]] link file name
				-n  [optional] runs the rad_nod step
				-o  [optional; default run_ami_*.d[zt]] first P file name
				-R  [optional] to run RAM3D (ami3d) after finish
EOM
}

# ======================================================
#   Read in options and defaults
# ======================================================

datafolder="data"
amifolder="ami"
njobs=1
mail=0
lstfilename="list"
lnkfilename="link_ready_all"
fPfilename="run_ami_*"
runlinking=0
runfirstP=0
runradnod=0
rami3d=0
ptype="auto"

while getopts hd:a:p:mj:lz:fk:no:R OPT; do
	case "${OPT}" in
		h)	USAGE; exit ;;
		d)	datafolder=`echo ${OPTARG}`;;
		a)	amifolder=`echo ${OPTARG}`;;
		p)	phase=`echo ${OPTARG}`;;
		m)  mail=1;;
		j)  njobs=`echo ${OPTARG}`;;
		l)  runlinking=1; ptype="man";;
		z)	lstfilename=`echo ${OPTARG}`;;
		f)  runfirstP=1; ptype="man";;
		k)	lnkfilename=`echo ${OPTARG}`;;
		n)  runradnod=1; ptype="man";;
		o)  fPfilename=`echo ${OPTARG}`;;
		R)  rami3d=1;;
		\?)     echo "Option "$OPT" not recognized...";echo ""; echo ""; USAGE; exit;;
	esac
done

# ======================================================
#--- run safeguards
# ======================================================

if [ ! -d $datafolder ]; then
	echo "ERROR: data directory does not exist"
	exit
fi

if [ ${phase} == "none" ] || [ ${phase} != "R" ] && [ ${phase} != "L" ]; then
	echo "ERROR: phase has to be (R)ayleigh or (L)ove"
	exit
fi

[ ! -d ${amifolder} ] && mkdir -p ${amifolder}

# ======================================================
#-- safeguard to avoid hyperthreading
# ======================================================
CPUnum=`cat /proc/cpuinfo | awk '/^processor/{print $3}' | wc -l`
[ $njobs -gt $CPUnum ] && echo "ERROR: requesting more parallel jobs than available processors; exit" && exit 

# ======================================================
#-- define functions
# ======================================================

datestmp=$(date +%Y%m%d-%H%M%S)

LOG () {
	#-----------------------------------------------
	#   log information to logfile
	[ ! -d ${amifolder}/logs ] && mkdir -p ${amifolder}/logs
	echo "${1}" >> ${amifolder}/logs/00ami_dir_parallelizer_${phase}_${datestmp}.log
}

LOGHEAD () {

		in=$1
		out=$2
		log=$3
		echo ' ' >> ${log}
		date >> ${log}
		echo `basename $0` $* >> ${log}
		echo ' ' >> ${log}
		printf 'Input List : %s\n' $in >> ${log}
		printf 'Output List: %s\n' $out >> ${log}
		echo "Datset directory: "$datafolder >> ${log}
		echo "Ami directory: "$amifolder >> ${log}
		echo "Selected Phase: "$phase >> ${log}
		echo ""
		echo "=============================================================" >> ${log}
}

ABSPATH () {
	#-----------------------------------------------
	#   get absolute path to directory
	echo "$(cd "$(dirname "$1")"; pwd)/$(basename "$1")"
}

# ======================================================

amifolder=`ABSPATH ${amifolder}`

#run dz files for Rayleigh and dt files for Love
if [ ${phase} == "R" ]; then
	ext=".dz"
	phasedir="Rayleigh"
	Ptype="R"
	[ ! -d ${amifolder}/${phasedir} ] && mkdir -p ${amifolder}/${phasedir}

elif [ ${phase} == "L" ]; then
	ext=".dt"
	phasedir="Love"
	Ptype="L"
	[ ! -d ${amifolder}/${phasedir} ] && mkdir -p ${amifolder}/${phasedir}

fi

# ======================================================
#-- main code
# ======================================================

#-- set date
datenow=`date "+%d-%b-%Y"`
timenow=`date "+%H:%M:%S"`
stime=`date "+%s"`

LOG ""
LOG "============================================================="
LOG ""
LOG ${datestmp}
LOG `basename $0` $*
LOG ""
LOG "Input List: ${datafolder}/lists/${lstfilename}${ext}"
LOG "Datset directory: $datafolder"
LOG "Ami directory: $(basename $amifolder)"
LOG "Selected Phase: $phase"
LOG "-------------------------------------------------------------"
LOG ""
LOG ""

datafolder=`ABSPATH ${datafolder}`

####################################################
#run  make_ami_link_input
####################################################

if [ ${ptype} == "auto" ] || [ ${runlinking} == 1 ]; then

		#--check if dz list file exists and is not empty
		#IN DZ FILE
		lstfile=`echo ${datafolder}/lists/${lstfilename}${ext}`

		[[ ! -f ${lstfile} || ! -s ${lstfile} ]] && LOG "    ERROR: No or empty input list" && exit 

		lnkfile=`echo ${datafolder}/lists/${lnkfilename}${ext}`

		#--create link file
		cat ${lstfile} | awk '{print $1}' | grep ${ext} \
				| awk -F "/$(basename ${datafolder})/" '{print $2}' | awk -F '/' '{OFS="/"; print $1,$2,$3,$4,$5}' \
				| sort | uniq > ${lnkfile}

		#--check if link file is created
		[[ ! -f ${lnkfile} || ! -s ${lnkfile} ]] && LOG "    ERROR: Link list empty/not created" && exit

		LOG "    ~Step 1: Creating list of files to link"
		LOG "      $(basename ${lnkfile}) list created with $(wc -l ${lnkfile} | awk '{print $1}') lines."
		LOG ""

		####################################################
		#run link_ami_dirs_1s
		#link_ami_dirs_1s -data data -ami ami -phase R -file data/lists/link_ready_all.dz &
		####################################################

		ladfolder=`echo ${amifolder}/${phasedir}/lists/linkamidirs`

		#--create linkamidirs folder (if does not exist)
		[ ! -d ${ladfolder} ] && mkdir -p ${ladfolder}

		outlnkfile=`echo $ladfolder/${lnkfilename}${ext}".linked"${ext}`

		LOG "    ~Step 2: Linking ami directories"

		#--start link dirs log file
		lnklogfile=`echo ${amifolder}/logs/linkamidirs_${phase}_${datestmp}.log`

		if [ -f ${lnklogfile} ]; then
				LOG "    Warning: link_dirs logfile "`basename ${lnklogfile}`" already exists. "
		fi

		LOGHEAD ${lnkfile} ${outlnkfile} ${lnklogfile}

		LOG "      detailed linking log:"
		LOG "        ${lnklogfile}"
		#--start looping through folders in the link list
		for line in $(cat ${lnkfile}); do

				#-- Decompose the line
				year=`echo $line | awk -F '/' '{print $1}'`
				month=`echo $line | awk -F '/' '{print $2}'`
				event=`echo $line | awk -F '/' '{print $3}'`
				AN_folder=`echo $line | awk -F '/' '{print $4}'`
				AN_folder_small=`echo $AN_folder | tr "[A-Z]" "[a-z]"`
				station=`echo $line | awk -F '/' '{print $5}'`
				net=`echo $station | awk -F . '{print $1}' | tr "[A-Z]" "[a-z]"`
				stn=`echo $station | awk -F . '{print $2}' | tr "[A-Z]" "[a-z]"`

				#-- output:
				printf '\n%4s %2s %13s %1s %s\n' $year $month $event $AN_folder $station >> $lnklogfile

				#-- set directories
				ddir=`echo ${datafolder}/$year/$month/$event/$AN_folder/$station`
				adir=`echo ${amifolder}/${phasedir}/$year/$month/$event/$AN_folder_small/$net.$stn`
				cmtf=`echo ${datafolder}/$year/$month/$event/"cmt"$event`

				#-- does the data folder exist?
				if [ ! -d ${ddir} ]; then
						printf '      Path does not exist! %s\n' `echo $year/$month/$event/$AN_folder/$station` >> $lnklogfile
						continue
				fi

				#-- check that the cmt file exists
				if [ ! -f ${cmtf} ]; then
						#printf '1, CMTFILE Missing, %s\n' `echo $cmtf`
						printf '      CMTFile Absent! %s\n' `echo $cmtf` >> $lnklogfile
						continue
				fi
				#-- move to the station folder
				printf '%s\n' $ddir >> $lnklogfile

				#-- generate a list of all displacement seismograms in the data directory
				dsgs=`ls $ddir | grep "${stn}${ext}"`
				printf 'displacement seismograms in the folder: %s\n' $dsgs >> $lnklogfile

				if [ `echo $dsgs | wc -w` == 0 ]; then
						printf '      DISP ABSENT... %s.%s\n' $stn $extn >> $lnklogfile
						continue
				fi

				#-- does the amidir folder exist?
				[ ! -d  ${adir} ] && mkdir -p ${adir}

				#-- check if there are displacement component seismograms in the amidir
				#if [ `ls ${adir} | grep -c ${ext}` > 0 ]; then
				#	printf '         DISP EXIST...skip\n' >> $lnklogfile
				#	continue
				#fi

				#-- symbollically link the displacement seismograms with the real ones (absolute paths!!)
				for dsg in $dsgs; do
						#rm -f $adir/$net.$stn.$extn
						ln -s ${ddir}/${dsg} ${adir}/${net}.${stn}${ext}
						printf '         Link %s -> %s.%s%s\n' $dsg $net $stn $extn >> $lnklogfile

						#-- add to output list of files
						printf '%s/%s.%s%s\n' $adir $net $stn $ext >> ${outlnkfile}

				done

				#--linking with the cmtfile also.
				ln -sf $cmtf ${adir}/cmt$event
				printf '         Link cmt%s\n'  $event >> $lnklogfile

		done #-- for each line (`cat $infile`)

		echo "Done" >> $lnklogfile

		#--check if link file is created
		[[ ! -f ${outlnkfile} || ! -s ${outlnkfile} ]] && LOG "      ERROR: Link output empty/not created" && exit

		LOG "      $(wc -l ${outlnkfile} | awk '{print $1}') files successfully linked!"
		LOG ""
fi

if [ ${ptype} == "man" ] && [ ${runlinking} == 0 ]; then

		LOG "   ~Running separate options selected; Skipping Directory Linking Step"
		ladfolder=`echo ${amifolder}/${phasedir}/lists/linkamidirs`
		outlnkfile=`echo ${ladfolder}/${lnkfilename}${ext}".linked"${ext}`
		#--check if link file exists
		[[ ! -f ${outlnkfile} || ! -s ${outlnkfile} ]] && LOG "      ERROR: No/empty Link output" && exit
fi

####################################################
#run make_ami_fistP_1s
#make_ami_firstP_1s <-ami amidir> <-phase R/L> <-years years>'
####################################################

if [ ${ptype} == "auto" ] || [  ${runfirstP} == "1" ]; then

		inlnklist=${outlnkfile}
		fPfolder=`echo ${amifolder}/${phasedir}/lists/firstP`
		fPfolderlist=`echo ${fPfolder}/$(basename ${inlnklist}).folderlist`

		#--create firstP folder (if does not exist)
		[ ! -d ${fPfolder} ] && mkdir -p ${fPfolder}

		#--create aux files: log, problems and success
		fPlogfile=`echo ${amifolder}/logs/firstP_${phase}_${datestmp}.log`
		fPpbmfile=`echo ${fPfolder}/problems_firstP_${datestmp}`
		fPscsfile=`echo ${fPfolder}/run_ami_${datestmp}${ext}`

		if [ -f ${fPlogfile} ]; then
				LOG "    Warning: firstP logfile "`basename ${fPlogfile}`" already exists. "
		fi

		LOGHEAD ${inlnklist} ${fPscsfile} ${fPlogfile}

		LOG "    ~Step 3: Creating firstP files"
		LOG "      detailed firstP log:"
		LOG "        ${fPlogfile}"

		#-- link to reference files
		inP=`echo $AMIINVHOME'/reference/inP_ttimes'`
		out=`echo $AMIINVHOME'/reference/out_ttimes'`

		#--create folder list
		sed 's%[a-z0-9]*\.[a-z0-9]*'${ext}'%%' ${inlnklist} > ${fPfolderlist}

		FIRSTP () {

				printf -v tmp "%0.10d" ${kk}
				#--compute event depth and distance
				dede=`(echo r ${inlst[$kk]}/*${ext}; echo lh EVDP GCARC; echo q) | sac | grep "[CP] =" | sort -u`
				evde=`echo $dede | awk '{print $3}'`
				evdi=`echo $dede | awk '{print $6}'`
				printf '    Event depth: %s; distance: %s\n' ${evde} ${evdi} >> .log${tmp}


				#-- are there enough records?
				if [ `echo $dede | wc -w` != 6 ]; then
						printf '       Event depth/distance Error! %s\n' >> .log${tmp}

						printf '0, Event depth/distance Error , %s\n' ${inlst[$kk]} >> .pbm${tmp}

						continue
				fi

				#-- compute the first P arrival time
				mkdir .times${tmp}
				(cd .times${tmp} && ( cat $inP; echo ${evde}; echo ${evdi}; cat $out ) | ttimes | grep "1  P" | grep "[0-9][0-9]" | cut -c 24-33 | head -1 > ${inlst[$kk]}/firstP)

				# -- output
				fP=`cat ${inlst[$kk]}/firstP`
				printf '    Event first P: %s\n' ${fP} >> .log${tmp}


				#-- output directory and file to the success or problem file
				if [ -f ${inlst[$kk]}/firstP ]; then
						printf '%s\n' ${inlst[$kk]}*${ext} >> .scs${tmp}

				else
						printf '1 , 1P FILE NOT CREATED , %s\n' ${inlst[$kk]}*${ext} >> .pbm${tmp}

				fi
		}

		inlst=(`cat ${fPfolderlist}`)
		#--run in parallel; first loop is for looping the outer steps of the list
		for ii in $(seq 0 $njobs ${#inlst[@]}); do
				kk=$ii
				printf '\n~running line %s of the list\n' $ii >> ${fPlogfile}

				#second loop is the parallellization
				for jj in $(seq 1 $njobs); do

						#break when the index is the last line
						[ $kk -gt $(echo "${#inlst[@]}-1"|bc -l) ] && echo "~last loop" >> ${fPlogfile} && break

						FIRSTP &
						kk=$((kk+1))

				done #parallelizer loop

				wait
				#to do:::: supress the error messages for when there is not a file
				cat .log?????????? >> ${fPlogfile} 2> /dev/null && rm .log??????????
				cat .pbm?????????? >> ${fPpbmfile} 2> /dev/null && rm .pbm??????????
				cat .scs?????????? >> ${fPscsfile} 2> /dev/null && rm .scs??????????
				rm  -r .times??????????

		done #file loop

		printf '\nDone!' >> ${fPlogfile}

		#--check if link file is created
		[[ ! -f ${fPscsfile} || ! -s ${fPscsfile} ]] && LOG "      ERROR: FistP success file empty/not created" && exit

		LOG "      $(wc -l ${fPscsfile} | awk '{print $1}') first P arrival times calculated!"
		LOG "      $(wc -l ${fPpbmfile} | awk '{print $1}') first P problems!"
		LOG ""
		
fi

if [ ${ptype} == "man" ] && [ ${runfirstP} == 0 ] ; then

		LOG "   ~Running separate options selected; Skipping First P step"
		fPfolder=`echo ${amifolder}/${phasedir}/lists/firstP`
		fPscsfile=`echo ${fPfolder}/${fPfilename}`
		
		#--check if first P file exists
		echo $fPscsfile
		[[ ! -f ${fPscsfile} || ! -s ${fPscsfile} ]] && LOG "      ERROR: No/empty FistP success file" && exit

fi

if [ ${ptype} == "auto" ] || [ ${runradnod} == 1 ]; then
		
		####################################################
		#run run_ami_rad_nod_1s
		#run_ami_rad_nod_1s -ami <amidir> -phase <R/L> -file <run_ami_YEAR.d[z/t]>
		####################################################

		inrnlist=${fPscsfile}
		rnfolder=`echo ${amifolder}/${phasedir}/lists/rad_nod`

		#--create rad_nod folder (if does not exist)
		[ ! -d ${rnfolder} ] && mkdir -p ${rnfolder}

		#--create aux files: log, problems and success
		rnlogfile=`echo ${amifolder}/logs/rad_nod_${phase}_${datestmp}.log`
		rnpbmfile=`echo ${rnfolder}/problems_rad_nod_${datestmp}`
		rnfolderlist=`echo ${rnfolder}/$(basename ${fPscsfile}).folderlist`

		if [ -f ${rnlogfile} ]; then
				LOG "    Warning: rad_nod logfile "`basename ${rnlogfile}`" already exists. "
		fi

		LOGHEAD ${inrnlist} ${rnfolder} ${rnlogfile}

		LOG "    ~Step 4: Creating rad_nod files"
		LOG "      detailed rad_nod log:"
		LOG "        ${rnlogfile}"

		#--create folder list
		sed 's%[a-z0-9]*\.[a-z0-9]*'${ext}'%%' ${inrnlist} > ${rnfolderlist}	

		RAD_NOD () {
				(echo $Ptype; echo .list${tmp}) | nod_all_prem > .log${tmp}
		}

		inlst=(`cat ${rnfolderlist}`)
		nlines=`echo ${#inlst[@]} $njobs | awk '{printf "%d", ($1/$2)+1}'`

		#--start parallel loop
		for ii in $(seq 0 ${nlines} ${#inlst[@]}); do
				#-- build the separate lists
				printf -v tmp "%0.10d" ${ii}
				awk -v a=$ii -v b=$nlines '{ if(NR>=a && NR<(a+b)) print $0}' ${rnfolderlist} > .list${tmp}
				echo "    temporary list${tmp} created" > .log${tmp}
				RAD_NOD &
		done
		wait
		#--clean up
		cat .log?????????? >> ${rnlogfile} && rm .log??????????
		cat .list?????????? >> ${rnlogfile} && rm .list??????????

		printf '\nDone!' >> ${rnlogfile}
		LOG "      Fund OK: "`grep -c "Fund OK;" ${rnlogfile}`
		LOG "      No F:    "`grep -c "; No F" ${rnlogfile}`
		LOG ""

		#-- determine end time
		etime=`date "+%s"`
		runtime=`echo $etime $stime | awk '{print ($1-$2)/3600}'`
		#LOG "Start: "$datenow" "$timenow
		#LOG "End: " date "+%d-%b-%Y %H:%M:%S"
		LOG "Running time "$runtime" hours"

		LOG "DONE CONGRATS YOUR DIRECTORIES ARE READY FOR RAM3N!!!!"
		LOG ""
		LOG "============================================================="
fi

if [ ${rami3d} == 1 ]; then
		[[ ! -f ${fPscsfile} || ! -s ${fPscsfile} ]] && LOG "      ERROR: No/empty FistP success file" && exit
		LOG " Running RAM3D option on; Starting."
		RAM3D -p ${phase} -a ${amifolder} -o ${fPfilename} -j ${njobs} -m &
		wait
		amiff=$amifolder/${phasedir}/lists/amiFits
		amifits=`echo ${amiff}/amiFITS_??????`
		LOG "-------------------------------------------------------------"
		LOG "AmiFITS:"`cat ${amifits} | wc -l | awk '{print $1}'`
		LOG "AmiFITS Errors:"`cat ${amifits}_errors | wc -l | awk '{print $1}'`
fi

#-- mail finished
if [ $mail == 1 ]; then
    rnum=`echo | awk 'BEGIN {srand()} {print int(rand()*100000)}'`
    echo `basename $0` $* > mail.comp.$1$extn.$rnum
    echo `hostname` >> mail.comp.$1$ext.$rnum
    echo $rundir >> mail.comp.$1$ext.$rnum
    echo "Start: "$datenow" "$timenow >> mail.comp.$1$ext.$rnum
    echo "End: " `date "+%d-%b-%Y %H:%M:%S"` >> mail.comp.$1$ext.$rnum
    echo "Runtime: " $runtime " hours" >> mail.comp.$1$ext.$rnum
    echo " " >> mail.comp.$1$ext.$rnum
    echo "." >> mail.comp.$1$ext.$rnum
    grep "^Station: " $outami | tail -n1 >> mail.comp.$1$ext.$rnum
    echo -n " Successful Fits: " >> mail.comp.$1$exn.$rnum
    cat $amifits | wc -l >> mail.comp.$1$ext.$rnum
    cat mail.comp.$1$ext.$rnum
    if [ -f /usr/bin/mail ]; then
        mail -s "run_ami3dl" $emailproc < mail.comp.$1$ext.$rnum
    elif [ -f /usr/bin/mutt ]; then
        mutt -s "run_ami3dl" $emailproc < mail.comp.$1$ext.$rnum
    fi
    rm -f mail.comp.$1$ext.$rnum
fi
