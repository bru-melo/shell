#!/usr/bin/env bash

#saclst knetwk f $stationdir/$event.$station.* 2>/dev/null | awk -F

headers=(NPTS B E IFTYPE  LEVEN DELTA IDEP DEPMIN DEPMAX DEPMEN KZDATE KZTIME IZTYPE KSTNM CMPAZ CMPINC STLA STLO STEL STDP EVLA EVLO EVDP KHOLE DIST AZ BAZ GCARC LOVROK USER1 USER9 NVHDR SCALE NORID NEVID LPSPOL LCALDA KCMPNM KNETWK)
#2013/12/131209_084727/b/xa.bhw02/
#2013/04/130414_013227/b/xa.bhn03/
dir=/mnt/seismodata2/BM/projects/Asia_XA_ETH/headers
projectname=test1
dir_old=/mnt/seismodata2/BM/projects/Asia_XA_ETH/data
dir_new=/mnt/seismodata2/BM/projects/Asia_XA_ETH/data_bk
year=2013
month=04
event=130414_013227
network=xa
station=bhn03
letter=b
tempfile=temp
hd=hh
file=$dir/headers_${event}_${network}.${station}_${hd}
n1=n
e2=e

for header in ${headers[@]};do
	hdz_old=`saclst ${header} f $dir_old/$year/$month/$event/$letter/$network.$station/*$station.${hd}z | awk '{print $2}'`
	hdz_new=`saclst ${header} f $dir_new/$year/$month/$event/$letter/$network.$station/*$station.${hd}z | awk '{print $2}'`
	
	hdn_old=`saclst ${header} f $dir_old/$year/$month/$event/$letter/$network.$station/*$station.${hd}$n1 | awk '{print $2}'`
	hdn_new=`saclst ${header} f $dir_new/$year/$month/$event/$letter/$network.$station/*$station.${hd}$n1 | awk '{print $2}'`

	hde_old=`saclst ${header} f $dir_old/$year/$month/$event/$letter/$network.$station/*$station.${hd}$e2 | awk '{print $2}'`
	hde_new=`saclst ${header} f $dir_new/$year/$month/$event/$letter/$network.$station/*$station.${hd}$e2 | awk '{print $2}'`
	
	echo "$header dz_old=$hdz_old de_old=$hde_old dn_old=$hdn_old" >> $tempfile
	echo "| dz_new=$hdz_new de_new=$hde_new dn_new=$hdn_new" >> $tempfile
#	echo " "
	
	diffz=""
	diffe=""
	diffn=""

	if [ "$hdz_old" != "$hdz_new" ]; then
		hdz="different"
		#diffz=`expr $hdz_old - $hdz_new`
		diffz=`echo $hdz_old - $hdz_new | bc`
	else
		hdz="same"
	fi
	
	if [ "$hde_old" != "$hde_new" ]; then
		hde="different"
		#diffe=`expr $hde_old - $hde_new`
		diffe=`echo $hde_old - $hde_new | bc`
	else
		hde="same"
	fi

	if [ "$hdn_old" != "$hdn_new" ]; then
		hdn="different"
		#diffn=`expr $hdn_old - $hdn_new`
		diffn=`echo $hdn_old - $hdn_new | bc`
	else
		hdn="same"
	fi

	echo "| $hdz $hde $hdn" >> $tempfile
	echo "| $diffz $diffe $diffn" >> $tempfile
	echo "--- " >> $tempfile

done

less $tempfile | column -t > $file

rm $tempfile
