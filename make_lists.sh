#!/usr/bin/env bash

#this code will loop over project folders (it is how I organized my data...),
#to make lists of all stations we requested (query_all.txt), the dzs we
#actually downloaded (data_dz.txt) and ami fits.

projects=`ls -d $proj/*`
ray=ami/Rayleigh/lists/amiFits
luv=ami/Love/lists/amiFits
luv2=ami2/Love/lists/amiFits
out1=query_all.txt
out2=data_station0.txt
out3=dz_ev.txt
out4=dt_ev.txt
out5=ray_ev.txt
out6=luv_ev.txt

printf "NET ST LAT LON\n" > $out1
printf "NET ST LAT LON #DZ #AmiR_Fits #AmiL_Fits #DT\n" > $out2
printf "EV NET.ST DIST MAG\n" > $out3
printf "EV NET.ST DIST MAG\n" > $out4
printf "EV NET.ST DIST MAG\n" > $out5
printf "EV NET.ST DIST MAG\n" > $out6

### LOOP IN ALL PROJ FOLDERS###

for pj in $projects
do
	###QUERY LIST###
	#query list will be used as source of station locations
	cat $pj/lists/*.stlst.csv | awk -F[,] '{print $1,$2,$9,$10}' >> $out1
	echo "########################################"
	echo $pj
	echo "      query list done"
	###############

	###DZ EVENT LIST###
	while read sac; do
		evz=`echo $sac | awk -F[/] '{print $10}'`
		stz=`echo $sac | awk -F[/] '{print $12}'`
		distz=`saclst DIST f $sac | awk '{print $2}'`
		magz=`saclst USER1 f $sac | awk '{print $2}'`
		printf "%s %s %s %s\n" $evz $stz $distz $magz >> $out3
	done < $pj/data/lists/list.dz
	echo "      dz list done"
	##############################

	###DT EVENT LIST###
	while read sac; do
		evt=`echo $sac | awk -F[/] '{print $10}'`
		stt=`echo $sac | awk -F[/] '{print $12}'`
		distt=`saclst DIST f $sac | awk '{print $2}'`
		magt=`saclst USER1 f $sac | awk '{print $2}'`
		printf "%s %s %s %s\n" $evt $stt $distt $magt >> $out4
	done < $pj/data/lists/list.dt
	echo "      dt list done"
	##############################

	if [ -d $pj/ami/Rayleigh/lists/amiFits ]; then
		echo "contains amiFits folder"
		cat $pj/$ray/amiFITS_???? > $pj/$ray/amiFITS_all
		cat $pj/$luv/amiFITS_???? > $pj/$luv/amiFITS_all
		#aditional ami folder for the wrong paths on the dt list
		cat $pj/$luv2/amiFITS_???? >> $pj/$luv/amiFITS_all

		#if running again consider selectiong stations from the query list
		station=`cat $pj/data/lists/list.dz | awk -F[/.] '{print ($13)}' | sort -u`

		###STATION LIST WITH AMIFIT INFO###
		for st in $station
		do
			n_dz=`grep $st $pj/data/lists/list.dz | wc | awk '{print $1}'`
			n_dt=`grep $st $pj/data/lists/list.dt | wc | awk '{print $1}'`
			n_afr=`grep $st $pj/$ray/amiFITS_???? | wc | awk '{print $1}'`
			n_afl=`grep $st $pj/$luv/amiFITS_???? | wc | awk '{print $1}'`
			net=`grep -w -i $st $out1 | awk 'NR==1 {print $1}'`
			lon=`grep -w -i $st $out1 | awk 'NR==1 {print $3}'`
			lat=`grep -w -i $st $out1 | awk 'NR==1 {print $4}'`
			printf "%s %s %s %s %s %s %s %s\n" $net $st $lon $lat $n_dz $n_afr $n_afl $n_dt>> $out2 
		done
		echo "      station list done"
		###################################

		###RAYFIT EVENT LIST### 
		while read ami; do
			evr=`echo $ami | awk -F[/] '{print $11}'`
			str=`echo $ami | awk -F[/] '{print $13}'`
			distr=`grep $evr $out3 | grep $str | awk '{print $3}'`
			magr=`grep $evr $out3 | grep $str | awk '{print $4}'`
			printf "%s %s %s %s\n" $evr $str $distr $magr >> $out5
		done < $pj/$ray/amiFITS_all
		echo "      ami rayleigh list done"
		#######################
		
		###LOVEFIT EVENT LIST###
		while read ami; do
			evl=`echo $ami | awk -F[/] '{print $11}'`
			stl=`echo $ami | awk -F[/] '{print $13}'`
			distl=`grep $evl $out4 | grep $stl | awk '{print $3}'`
			magl=`grep $evl $out4 | grep $stl | awk '{print $4}'`
			printf "%s %s %s %s\n" $evl $stl $distl $magl >> $out6
		done < $pj/$luv/amiFITS_all
		echo "      ami love list done"
		########################


	else
		#make the station information list anyway
		echo "   does not contain amiFits folder"
		for st in $station
		do
			n_dz=`grep $st $pj/data/lists/list.dz | wc | awk '{print $1}'`
			n_dt=`grep $st $pj/data/lists/list.dt | wc | awk '{print $1}'`
			net=`grep -w -i $st $out1 | awk 'NR==1 {print $1}'`
			lon=`grep -w -i $st $out1 | awk 'NR==1 {print $3}'`
			lat=`grep -w -i $st $out1 | awk 'NR==1 {print $4}'`
			printf "%s %s %s %s %s 0 0 %s\n" $net $st $lon $lat $n_dz $n_dt >> $out2
		done
		echo "      station list done"
	fi

done
echo "done!"

