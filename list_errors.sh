#!/usr/bin/env bash

#check lists information
#run from project folder

#proj=${proj_path}/Asia_2018_IRIS

projects=`ls -d -- */ $proj`
llog=$proj/list.log

printf "REMEMBER TO DRINK WATER\n\n" > $llog

for proj in $projects
do
	if [ -d "$proj/data/lists" ]; then
		#echo $proj
		ldz=$proj/data/lists/list.dz
		lerr=$proj/data/lists/list.errors

		printf "\n%s \n\n" $proj >> $llog
		#number of sucessful dz

		yeah=`cat $ldz | wc | awk '{print $1}'` 
		printf "     Number of sucessful dz: %s YEAH! \n" $yeah >> $llog

		#grep the different errors from error file
		probs=`cat $lerr | awk '{print $1}' | sort -u`
		#echo "$probs"
		printf "     Errors Found OH NO!\n" >> $llog

		#count how many of each problem
		for p in $probs
		do
			#echo $p
			mwa=`cat $lerr | awk '{print $1}' | grep $p | wc | awk '{print $1}'`
			#echo $mwa
			printf "       %s: %s\n" $p $mwa >> $llog
		done
	fi
done

printf "\n The end" >> $llog
